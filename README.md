# Qiskit Braket Plugin 
The [Amazon Braket Python SDK](https://github.com/aws/amazon-braket-sdk-python) is an open source library that provides a framework to interact with quantum computing hardware devices and simulators through Amazon Braket.

[Qiskit](https://qiskit.org) is an open-source framework for working with noisy intermediate-scale quantum computers (NISQ) at the level of pulses, circuits, and algorithms.

This project provides tools to access Amazon Braket devices directly in the Qiskit enviroment. Upon installing the package, costumers are able to code their quantum circuits, submit jobs to a AWS quantum device, and retrieve and analysis the results within the Qiskit framework. 

## Installation
Before working with this plugin, please make sure that you've installed Braket SDK and Qiskit. In order to work with the remote Amazon Braket devices, please set up your AWS account as instructed [here](https://github.com/aws/amazon-braket-sdk-python#prerequisites) too. 

**TODO**
## Dependency Diagram
The following figure shows the dependencies between files within 'qiskit_braket_provider'. Notice that all the classes defined in this part of the code are subclasses of the corresponding base class in Qiskit. Thus, the custumers are able to program/analize problems without a direct understanding of the Braket library. 
![](images/dependency.png)

## A short demonstration
We will now give a simply demonstration of how a qiskit user access a Braket device through things pluggin. 
### Setting up the AWS provider and backends 
First 
```
from qiskit_aws_plugin.aws_provider import AWSProvider
provider = AWSProvider()
```
Two subclasses of the Backend base class are defined in `braket_backend.py`. One of them is `BraketLocal`, which corresponds to the local simulator installed along with Braket SDK, 
```
dev_local = pro.get_backend()
```
The other one, `AWS_Backend`, acquires access to a remote AWS device if the name or the arn of this device is given by the user. 
```
dev_aws = pro.get_backend(Name = "SV1")
```
or
```
dev_aws = pro.get_backend(Arn = 'arn:aws:braket:::device/quantum-simulator/amazon/sv1')
```

### Executing your Qiskit quantum circuit on a Braket device
The next step is to actually run a program on a Braket device. One of essential procedures here is to translate a circuit coded by Qiskit to its counterpart in Braket, if the circuit if fully supported by the Braket device requested. For example, we construct the following circuit, 
```
from qiskit import QuantumCircuit
qcircuit = QuantumCircuit(3)
qcircuit.rx(pi/2, 0)
qcircuit.rxx(0.2,0,1)
qcircuit.ccx(0,1,2)
qcircuit.cp(0.1,0,1)
qcircuit.measure_all()
print (qcircuit)
```

```
        ┌─────────┐┌───────────┐                          ░ ┌─┐      
   q_0: ┤ RX(π/2) ├┤0          ├──■──────■─────────o──────░─┤M├──────
        └─────────┘│  RXX(0.2) │  │  ┌───┴────┐┌───┴────┐ ░ └╥┘┌─┐   
   q_1: ───────────┤1          ├──■──┤ P(0.1) ├┤ P(0.1) ├─░──╫─┤M├───
                   └───────────┘┌─┴─┐└────────┘└────────┘ ░  ║ └╥┘┌─┐
   q_2: ────────────────────────┤ X ├─────────────────────░──╫──╫─┤M├
                                └───┘                     ░  ║  ║ └╥┘
meas: 3/═════════════════════════════════════════════════════╩══╩══╩═
                                                             0  1  2 
```

We then submit the quantum job to the device we obtained above by running 
```
job = dev_local.run(qc)
```
for the local Braket simulator. Or
```
job = dev_aws.run(qc,s3_folder = ("amazon-braket-Your-Bucket-Name", "folder-name"))
```
for a AWS device, where costumers need to provider their s3_folder information to store the result. The user can set shots to a desired number of experiments. The default value is 100. 

We can check that the translation of the quantum circuit from Qiskit to Braket works properly by printing the attribute `job.braket_circuit`, 
```
T  : |   0    |   1   |2|    3     |     4      |
                                                 
q0 : -Rx(1.57)-XX(0.2)-C-C----------C------------
               |       | |          |            
q1 : ----------XX(0.2)-C-PHASE(0.1)-PHASE01(0.1)-
                       |                         
q2 : ------------------X-------------------------

T  : |   0    |   1   |2|    3     |     4      |
```

### Retriving Results 
Finally, the results can be retrived by calling `job.result()`. When running programs on a remote AWS device, users can specify their own polling time and polling interval by modifying the parameters as 
```
result = job.result(timeout=poll_timeout_seconds, wait=poll_interval_seconds)
```
The returning results have type `qiskit.result.result.Result`. Calling the following method 
```
result.get_counts()
```
yields
```
{'001': 42, '000': 55, '010': 1, '111': 2}
```
Notice that by Qiskit conventions, the bits are labelled from right to left, which is the opposite of Braket's conventions. The order of the qubits has been adjusted for Qiskit conventions. 

## Next Steps

1. Qiskit-to-Braket circuit translator:
 - [ ] Import lists of standard bracket gates dynamically 
 - [ ] If there's a Braket gate not available in Qiskit, we might want to create the Qiskit version and offer it to the costumer. 
 - [ ] Other qiskit operations/algorithms? 
2. Braket-to-Qiskit result translator:
 in braket: Amplitude, Expectation, Probability, Sample, Variance
 in qiskit: snapshots (dict), memory (list), unitary (list or numpy.array), kwargs (any): additional data key-value pairs.
- [x] in aws_job.py, **TODO**: Need to make sure that we've translated all bracket results into qiskit results
    - [x] Result_types in braket are specified when the circuit is built. So further modifications are needed in qiskit_to_braket.py when translating the qiskit circuit into braket circuit. Eg. `backend = Aer.get_backend('statevector_simulator')`

# Other funtionalities 
It was mentioned in [this paper](https://arxiv.org/pdf/1809.03452.pdf) that the certain operations on a quantum device could be extended. These include fully generic classical processing, closed- loop feedback, and reconfiguration of the device (e.g., changing a magnetic field) on timescales longer than a single experiment.





## References: 
1. [Qiskit Terra API Reference: Providers Interface](https://qiskit.org/documentation/apidoc/providers.html)
2. [Qiskit Backends](https://medium.com/qiskit/qiskit-backends-what-they-are-and-how-to-work-with-them-fb66b3bd0463))
3. [Qiskit-AQT Provider](https://github.com/qiskit-community/qiskit-aqt-provider/blob/master/qiskit_aqt_provider/aqt_job.py)
4. [Braket ResultTypes](https://docs.aws.amazon.com/braket/latest/developerguide/braket-result-types.html)
5. [Getting AWS devices in Braket](https://github.com/aws/amazon-braket-examples/blob/main/examples/braket_features/Getting_Devices_and_Checking_Device_Properties.ipynb)




