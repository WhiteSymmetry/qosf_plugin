import warnings

from braket.circuits import Circuit, Gate, Instruction, Observable
from numpy import pi
from qiskit.circuit.controlledgate import ControlledGate

## TODO: load BRAKET_STANDARD_GATES dynamically


### BRAKET_STANDARD_GATES
# A list of standard gates supported by Braket
# Notice that any gate below has no more than one parameter.
BRAKET_STANDARD_GATES = [
    "ccnot",
    "cnot",
    "cphaseshift",
    "cphaseshift00",
    "cphaseshift01",
    "cphaseshift10",
    "cswap",
    "cy",
    "cz",
    "h",
    "i",
    "iswap",
    "pswap",
    "phaseshift",
    "rx",
    "ry",
    "rz",
    "s",
    "si",
    "swap",
    "t",
    "ti",
    "unitary",
    "v",
    "vi",
    "x",
    "xx",
    "xy",
    "y",
    "yy",
    "z",
    "zz",
]

### BRAKET_TO_QISKIT_DICT
# A dictionary in which the keys are names of gates in Braket, and the values are names of gates in Qiskit
# Used in aws_backend to initialize backend configuration
BRAKET_TO_QISKIT_DICT = {gate: gate for gate in BRAKET_STANDARD_GATES}
BRAKET_TO_QISKIT_DICT["cnot"] = "cx"
BRAKET_TO_QISKIT_DICT["ccnot"] = "ccx"
BRAKET_TO_QISKIT_DICT["cphaseshift"] = "cp"
# other braket cphaseshift gates are implemented by specifying target-bit, control-bit and ctrl-state
BRAKET_TO_QISKIT_DICT["cphaseshift00"] = ("cp", "00")
BRAKET_TO_QISKIT_DICT["cphaseshift01"] = ("cp", "01")
BRAKET_TO_QISKIT_DICT["cphaseshift10"] = ("cp", "10")
BRAKET_TO_QISKIT_DICT["pswap"] = "cswap"
BRAKET_TO_QISKIT_DICT["phaseshift"] = "p"
BRAKET_TO_QISKIT_DICT["v"] = "sx"
# si vi ti are the inverses of s, v, t gates
BRAKET_TO_QISKIT_DICT["vi"] = "sx"
BRAKET_TO_QISKIT_DICT["ti"] = "t"
BRAKET_TO_QISKIT_DICT["si"] = "s"
BRAKET_TO_QISKIT_DICT["xx"] = "rxx"
BRAKET_TO_QISKIT_DICT["yy"] = "ryy"
BRAKET_TO_QISKIT_DICT["zz"] = "rzz"
# xy not implemented in Qiskit


### QISKIT_TO_BRAKET_DICT[]
# A dictionary in which the keys are names of gates in Qiskit, while the values are Braket gates.
QISKIT_TO_BRAKET_DICT = {
    "ccx": Gate.CCNot,
    "cnot": Gate.CNot,
    "cx": Gate.CNot,
    "cp": Gate.CPhaseShift,
    "cswap": Gate.CSwap,
    "cy": Gate.CY,
    "cz": Gate.CZ,
    "h": Gate.H,
    "i": Gate.I,
    "iswap": Gate.ISwap,
    "pswap": Gate.PSwap,
    "phaseshift": Gate.PhaseShift,
    "rx": Gate.Rx,
    "ry": Gate.Ry,
    "rz": Gate.Rz,
    "s": Gate.S,
    "swap": Gate.Swap,
    "t": Gate.T,
    "sx": Gate.V,
    "x": Gate.X,
    "rxx": Gate.XX,
    "y": Gate.Y,
    "yy": Gate.YY,
    "z": Gate.Z,
    "rzz": Gate.ZZ,
}


def map_names_of_braket_gates_to_qiskit_gates(gates):
    # Arg: a list of strings representing the names of the gates supported by Braket
    # Rturn: a list of strings, which are the Qiskit names of the gates given by the argument
    q_gates = []
    for gate in gates:
        if BRAKET_TO_QISKIT_DICT[gate] not in q_gates:
            q_gates.append(BRAKET_TO_QISKIT_DICT[gate])
    return q_gates


def map_qiskit_circuit_to_braket_circuit(circuit, supported_gates):
    # Arg:
    # circuit: a Qiskit circuit to be translated
    # supported_gates: a list of strings, which are the names of gates supported by the Braket device requested
    # Return:
    # braket_circ: the circuit translated into Braket.
    # Errors will be raised if a gate in the input Qiskit circuit does not have a counterpart supported by the device requested
    braket_circ = Circuit()
    meas = [0] * circuit.num_qubits
    for instruction in circuit.data:
        inst_gate, qargs, cargs = instruction
        target_qubits = [bit.index for bit in qargs]
        if inst_gate.name in supported_gates:
            braket_gate = None
            if isinstance(inst_gate, ControlledGate):
                if inst_gate.name == "cp" and inst_gate.ctrl_state == 1:
                    braket_gate = Gate.CPhaseShift(inst_gate.params[0])
                elif (
                    inst_gate.name == "cp"
                    and inst_gate.ctrl_state == 0
                    and ("cp", "01") in supported_gates
                ):
                    braket_gate = Gate.CPhaseShift01(inst_gate.params[0])
                elif inst_gate.ctrl_state == 0:
                    raise ValueError(
                        "A '%s' gate controlled by the zero state is not supported by the device requested."
                        % inst_gate.name
                    )
            if not braket_gate:
                if inst_gate.params:
                    braket_gate = QISKIT_TO_BRAKET_DICT[inst_gate.name](
                        inst_gate.params[0]
                    )
                else:
                    braket_gate = QISKIT_TO_BRAKET_DICT[inst_gate.name]()
            instr = Instruction(braket_gate, target_qubits)
            braket_circ.add_instruction(instr)
        elif inst_gate.name == "barrier":
            warnings.warn("Barriers are not supported by the device requested.")
            continue
        elif inst_gate.name == "measure":
            meas[instruction[1][0].index] = 1
            continue
        else:
            raise KeyError(
                "Operation '%s' is not supported by the device requested"
                % inst_gate.name
            )
    if sum(meas) == 0:
        raise ValueError("Circuit must have at least one measurements.")
    if sum(meas) < circuit.num_qubits:
        warnings.warn(
            "The device requested supports measurements over all qubits only."
        )
    return braket_circ
