import warnings

import braket._sdk as braket_sdk
from braket.aws import AwsDevice, AwsDeviceType
from braket.devices import LocalSimulator
from qiskit.exceptions import QiskitError
from qiskit.providers import BackendV1 as Backend
from qiskit.providers import Options
from qiskit.providers.models import BackendConfiguration

from .braket_job import Braket_Job
from .qiskit_to_braket import (BRAKET_STANDARD_GATES,
                               map_names_of_braket_gates_to_qiskit_gates)


class BraketLocal(Backend):
    """
    Accessing the local simulator
    """

    def __init__(self, provider,name=None):
        if name == None:
            raise QiskitError(
               "Please specify which local device you would like to access to."
            )
        elif name == "simulator":
            self._device = LocalSimulator()
        else:
            raise QiskitError(
               "The local device %s you requested is not available." % name
            )
        configuration = {
            "backend_name": self._device.name, 
            "backend_version": "amazon-braket-sdk: " + braket_sdk.__version__,
            "url": None,
            "simulator": isinstance(self._device, LocalSimulator), 
            "local": True ,
            "coupling_map": None,
            "description": "Braket default local simulator",
            "basis_gates": map_names_of_braket_gates_to_qiskit_gates(
               [g.lower() for g in self._device.properties.action.get("braket.ir.jaqcd.program").supportedOperations] 
            ),
            "memory": False,
            "n_qubits": self._device.properties.paradigm.qubitCount,
            "conditional": False,
            "max_shots": max(
                [
                    r_type.maxShots
                    for r_type in self._device.properties.action.get(
                        "braket.ir.jaqcd.program"
                    ).supportedResultTypes
                ]
            ),
            "max_experiments": 1,
            "open_pulse": False,
            "gates": [{"name": "TODO", "parameters": [], "qasm_def": "TODO"}],
            "result_types": self._device.properties.action.get(
                "braket.ir.jaqcd.program"
            ).supportedResultTypes  # a list of result types supported on this device 
        }
        super().__init__(
            configuration=BackendConfiguration.from_dict(configuration),
            provider=provider,
        )

    @classmethod
    def _default_options(cls):
        return Options(shots=100)

    def run(self, circuit, **kwargs):
        # TODO:  other quantum objects
        for kwarg in kwargs:
            if kwarg != "shots":
                warnings.warn(
                    "Option %s is not used by this backend" % kwarg,
                    UserWarning,
                    stacklevel=2,
                )
        out_shots = kwargs.get("shots", self.options.shots)
        if out_shots > self.configuration().max_shots:
            raise ValueError(
                "Number of shots is larger than maximum " "number of shots"
            )
        job = Braket_Job(self, qcircuit=circuit, shots=out_shots)
        return job


# ref: https://docs.aws.amazon.com/braket/latest/developerguide/braket-devices.html#braket-simulator-sv1
# Amazon Braket provides two managed simulators: SV1 and TN1.
# -- SV1 calculates and keeps track of the full state vector evolution, supporting simulations of circuits with up to 34 qubits.
# -- TN1 is a tensor-network simulator, where each gate in a circuit is represented as a tensor.
# QPUs ...


class AWS_Backend(Backend):
    """
    Accessing an AWS device.
    """

    def __init__(self, provider, name=None, arn=None):
        """
        Args: if an arn of an AWS device is specified, it is used for requesting a device
              if a name of a device is provided instead, the name is used.
              if either is provided, an error will be thrown.
        """
        if arn:
            self._device = AwsDevice(arn)
            if name and name != self._device.name:
                raise QiskitError(
                    "The name and the arn of the device you provided do not match."
                )
        elif name:
            list_device = AwsDevice.get_devices(names=name)
            if bool(list_device):
                self._device = list_device[0]
            else:
                raise QiskitError(
                    "The device your requested is not available at your current location."
                )
        else:
            raise QiskitError(
                "Please specify which AWS device you would like to access to."
            )
        configuration = {
            "backend_name": self._device.name,
            "backend_version": "Updated at " + str(self._device.properties.service.updatedAt), 
            "arn": self._device.arn,
            "aws_session": self._device._aws_session,
            "simulator": self._device._type == AwsDeviceType.SIMULATOR,
            "local": False,
            "_topology_graph": self._device.topology_graph,
            "description": self._device.properties.service.deviceDocumentation.summary,
            "basis_gates": map_names_of_braket_gates_to_qiskit_gates(
                self._device.properties.action.get(
                    "braket.ir.jaqcd.program"
                ).supportedOperations
            ),
            "n_qubits": self._device.properties.paradigm.qubitCount,
            "memory": False,  # True if the backend supports memory
            "conditional": False,  # True if the backend supports conditional operations
            "coupling_map": None,  # The coupling map for the device
            "max_shots": max(
                [
                    r_type.maxShots
                    for r_type in self._device.properties.action.get(
                        "braket.ir.jaqcd.program"
                    ).supportedResultTypes
                ]
            ),
            "max_experiments": 1,  # The maximum number of experiments per job
            "open_pulse": False,  # True if the backend supports OpenPulse
            "gates": [
                {"name": "TODO", "parameters": [], "qasm_def": "TODO"}
            ],  # The list of GateConfig objects for the basis gates of the backend
            "result_types": self._device.properties.action.get(
                "braket.ir.jaqcd.program"
            ).supportedResultTypes  # a list of result types supported on this device
        }
        super().__init__(
            configuration=BackendConfiguration.from_dict(configuration),
            provider=provider,
        )

    @classmethod
    def _default_options(cls):
        # TODO: choose default options properly
        # TODO: options other than the number of shots, such as s3_folder, memory?
        return Options(shots=100)

    def run(self, circuit, **kwargs):
        # TODO: kwargs, other quantum objects
        for kwarg in kwargs:
            if kwarg != "shots" and kwarg != "s3_folder":
                warnings.warn(
                    "Option %s is not used by this backend" % kwarg,
                    UserWarning,
                    stacklevel=2,
                )
        out_shots = kwargs.get("shots", self.options.shots)
        S3_folder = kwargs.get("s3_folder", None)
        if not S3_folder:
            raise Exception("Please specify S3 location for your job")
        if out_shots > self.configuration().max_shots:
            raise ValueError(
                "Number of shots is larger than maximum " "number of shots"
            )
        job = Braket_Job(self, s3_folder=S3_folder, qcircuit=circuit, shots=out_shots)
        return job
