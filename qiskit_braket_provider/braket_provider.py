from qiskit.providers import ProviderV1

from .braket_backend import AWS_Backend, BraketLocal


class Braket_Provider(ProviderV1):
    def __init__(self):
        super().__init__()
        self.access_token = None
        self.name = "aws_provider"
        # Populate the list of AWS backends
        # self.backends = [BraketLocal(provider = self),AWS_Backend(name = "SV1",provider=self),AWS_Backend(name = "TN1",provider=self)]
        self.list_of_backends = ["SV1"]

    def __str__(self):
        return "<AWSProvider(name={})>".format(self.name)

    def __repr__(self):
        return self.__str__()

    def get_backend(self, local, Name=None, Arn=None, filters=None, **kwargs):
        if not local:
            return AWS_Backend(name=Name, arn=Arn, provider=self)
            # backends = [
            #    backend for backend in self.backends if backend.name() == name]
            # return filter_backends(backends, filters=filters, **kwargs)
        return BraketLocal(name=Name, provider=self)

    def backends(self, filters=None, **kwargs):
        return [self.get_backend()] + [
            self.get_backend(Name=backend_name)
            for backend_name in self.list_of_backends
        ]
