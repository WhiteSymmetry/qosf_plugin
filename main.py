from numpy import pi
from qiskit import QuantumCircuit

from qiskit_braket_provider.braket_provider import Braket_Provider

qc = QuantumCircuit(3)
qc.rx(pi / 2, 0)
# qc.rx(0, 1)
# qc.rx(0,2)
qc.rxx(0.2, 0, 1)
qc.ccx(0, 1, 2)
qc.cp(0.1, 0, 1)
qc.cp(0.1, 0, 1, ctrl_state=0)
# qc.rx(pi/2, 0)
qc.measure_all()
print(qc)

pro = Braket_Provider()
print("Test local simulator")
devlocal = pro.get_backend(local = True, Name = "simulator")
job = devlocal.run(qc, shots=10)
print(job.braket_circuit)
local_result = job.result()
print(local_result)
print(local_result.get_counts())

print("Test managed devices: SV1")
# dev = pro.get_backend(local = False, Name = "SV1")
dev = pro.get_backend(local = False, Arn="arn:aws:braket:::device/quantum-simulator/amazon/sv1")
job = dev.run(qc, s3_folder=("amazon-braket-aikeliu", "AKIA5GRAH3Y7POISRTUS"))
result = job.result(timeout=40000, wait=3)
print(result)

print("Test managed devices: TN1")
dev2 = pro.get_backend(local = False, Name = "TN1")
# dev4 = pro.get_backend(local = False, Arn = 'arn:aws:braket:::device/quantum-simulator/amazon/tn1')
job2 = dev.run(qc,s3_folder = ("amazon-braket-aikeliu","AKIA5GRAH3Y7POISRTUS"))
# result2 = job2.result()
