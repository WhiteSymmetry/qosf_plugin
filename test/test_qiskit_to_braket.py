import unittest

from braket.circuits import Circuit
from numpy import pi
from qiskit import QuantumCircuit
from qiskit_braket_provider.qiskit_to_braket import (
    BRAKET_STANDARD_GATES, map_names_of_braket_gates_to_qiskit_gates,
    map_qiskit_circuit_to_braket_circuit)

braket_gates_in_qiskit = map_names_of_braket_gates_to_qiskit_gates(
    BRAKET_STANDARD_GATES
)


class TestTranslator(unittest.TestCase):

    # This test expect an error as measurements are required for each circuit written in qiskit
    def test_empty_circuit(self):
        qc = QuantumCircuit(1)
        self.assertRaises(
            ValueError, map_qiskit_circuit_to_braket_circuit, qc, braket_gates_in_qiskit
        )

    def test_just_measure_circuit(self):
        qc = QuantumCircuit(1)
        qc.measure_all()
        braket_circ = Circuit()
        self.assertEqual(
            braket_circ,
            map_qiskit_circuit_to_braket_circuit(qc, braket_gates_in_qiskit),
        )
        self.assertWarns(
            UserWarning,
            map_qiskit_circuit_to_braket_circuit,
            qc,
            braket_gates_in_qiskit,
        )

    # This test expects an error as Braket supports measurements over all qubits only
    def test_partial_measurement(self):
        qc = QuantumCircuit(2, 1)
        qc.measure(0, 0)
        self.assertWarns(
            UserWarning,
            map_qiskit_circuit_to_braket_circuit,
            qc,
            braket_gates_in_qiskit,
        )

    # This test expects an error due to a qiskit gate that is not supported in Braket
    def test_invalid_gates_in_circuit(self):
        qc = QuantumCircuit(2)
        qc.rzx(0.1, 0, 1)
        qc.measure_all()
        self.assertRaises(
            KeyError, map_qiskit_circuit_to_braket_circuit, qc, braket_gates_in_qiskit
        )

    # This test expects an error due to a control state that is not supported in Bracket
    def test_invalid_control_gates_in_circuit(self):
        qc = QuantumCircuit(2)
        qc.cy(0, 1, ctrl_state=0)
        qc.measure_all()
        self.assertRaises(
            ValueError, map_qiskit_circuit_to_braket_circuit, qc, braket_gates_in_qiskit
        )

    def test_with_single_rx(self):
        qc = QuantumCircuit(1)
        qc.rx(pi, 0)
        qc.measure_all()
        braket_circ = Circuit().rx(0, pi)
        self.assertEqual(
            braket_circ,
            map_qiskit_circuit_to_braket_circuit(qc, braket_gates_in_qiskit),
        )

    def test_with_multiple_gates(self):
        qc = QuantumCircuit(3)
        qc.rx(pi, 0)
        qc.rxx(0.2, 0, 1)
        qc.ccx(2, 1, 0)
        qc.cp(0.1, 0, 1)
        qc.cp(0.1, 0, 1, ctrl_state=0)
        braket_circ = (
            Circuit()
            .rx(0, pi)
            .xx(0, 1, 0.2)
            .ccnot(2, 1, 0)
            .cphaseshift(0, 1, 0.1)
            .cphaseshift01(0, 1, 0.1)
        )
        qc.measure_all()
        self.assertEqual(
            braket_circ,
            map_qiskit_circuit_to_braket_circuit(qc, braket_gates_in_qiskit),
        )


if __name__ == "__main__":
    unittest.main()
