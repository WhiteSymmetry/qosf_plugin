import unittest
from qiskit_aws_plugin.aws_provider import AWSProvider


class TestProvider(unittest.TestCase):

    def test_provider_autocomplete(self):
        """Verifies that provider.backends autocomplete works.
        """
        # Not sure of the purpose of this test..
        pro = AWSProvider('123456')

        for backend in pro.backends:
            print (pro.backends[0])
            print (backend.name())
            print (hasattr(pro.backends[0], 'local_simulator'))
            self.assertTrue(hasattr(pro.backends, backend.name()))

    def test_provider_getbackend(self):
        """Verifies that provider.get_backend works.
        """
        pro = AWSProvider('123456')
        for backend in pro.backends:
            self.assertTrue(backend == pro.get_backend(backend.name())[0])

    def test_provider_localsimulator(self):
        pro = AWSProvider('123456')
        dev = pro.get_backend()[0]
        self.assertTrue(dev == pro.get_backend('local_simulator')[0])

#class TestProvider(unittest.TestCase):
#    def test_provider_localsimulator(self):
#        pro = AWSProvider('123456')
#        dev = pro.get_backend()
#        self.assertTrue(dev.name == "DefaultSimulator")
#    def test_provider_remote(self):
#        pro = AWSProvider('123456')
#        for backend in pro.backends:
#            self.assertTrue(backend+" Device" == (pro.get_backend(backend)[0]).name)    
            
